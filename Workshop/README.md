# Vorgehensweise

- npm init
- npm i express socket.io
- npm i nodemon --save-dev
- Anpassung package.json (type module und dev script)

- Erstellung index.js
	- Erstellung Express Server
	- public ordner als static Folder ausliefern
	- Hinzufügen von Socket.io Instanz zu Express
	- io.on('connection') => Auf Verbindungen hören und testen
    - users.js implementieren, um mit Benutzern zu arbeiten
    - disconnect und login implementieren
    - sendMessage und typing abhören

- Erstellung Frontend
	- public/index.html erstellen
	- public/chat.html erstellen
	- style.css abgeben und einfügen
	- main.js erstellen
		- wenn kein Name/raum => umleiten
		- socket.io Instanz erstellen mit: const socket = io();
		- check backend console => Neue Verbindung
		- Login Event emittieren =>  damit alle wissen, neuer user ist da.
        - Lokale Event-Listener erfassen (submit message, keypress)
        - Socket.io Listeners (notification, message, typing)