import express from "express";
import { createServer } from "http";
import { Server } from "socket.io";
import { addUser, getUser, deleteUser, getUsers } from "./users.js";

const app = express();
const server = createServer(app);
const io = new Server(server);

app.use(express.static("public"));

// Der Server lauscht auf Port 3000 für eingehende Verbindungen
server.listen(3000, () => console.log("Hört auf *:3000"));

// Socket.io wartet auf neue Verbindungen
io.on("connection", (socket) => {
  console.log("Ein Benutzer hat sich verbunden");

  // Event-Handler für das Abmelden eines Benutzers (Verbindungstrennung)
  socket.on("disconnect", async () => {
    console.log("Benutzer abgemeldet");
    const user = await deleteUser(socket.id); // deleteUser ist asynchron
    if (user) {
      // Sende eine Benachrichtigung an den Raum, in dem der Benutzer war
      socket
        .to(user.room)
        .emit("notification", {
          title: "Jemand hat den Raum verlassen",
          description: `${user.name} hat den Raum verlassen`,
        });
      // Aktualisiere die Benutzerliste im Raum
      socket.to(user.room).emit("users", await getUsers(user.room)); // getUsers ist asynchron
    }
  });

  // Event-Handler für das Einloggen eines Benutzers
  socket.on("login", async ({ name, room }, callback) => {
    const { user } = await addUser(socket.id, name, room); // addUser ist asynchron

    socket.join(user.room);
    // Sende eine Benachrichtigung an den Raum, dass ein neuer Benutzer angekommen ist
    socket
      .to(room)
      .emit("notification", {
        title: "Jemand ist hier",
        description: `${user.name} hat den Raum betreten`,
      });
    callback(user);
  });

  // Event-Handler für das Senden von Nachrichten
  socket.on("sendMessage", async (msg) => {
    const user = await getUser(socket.id); // getUser ist asynchron
    if (user) {
      // Sende die Nachricht an alle Benutzer im Raum, einschliesslich dem Absender
      io.in(user.room).emit("message", { user: user.name, text: msg });
    }
  });

  // Event-Handler für das Tippen von Nachrichten (Echtzeit-Tipp-Anzeige)
  socket.on("typing", async (data) => {
    const user = await getUser(socket.id); // getUser ist asynchron
    if (user) {
      // Benachrichtige alle anderen Benutzer im Raum, dass jemand tippt
      socket.to(user.room).emit("typing", data);
    }
  });
});
