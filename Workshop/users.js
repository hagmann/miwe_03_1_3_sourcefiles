const users = []

export const addUser = async (id, name, room) => {
    const user = { id, name, room }
    users.push(user)
    return { user }
}

export const getUser = async id => {
    let user = users.find(user => user.id == id)
    return user
}

export const deleteUser = async (id) => {
    const index = users.findIndex((user) => user.id === id);
    if (index !== -1) return users.splice(index, 1)[0];
}

export const getUsers = async (room) => users.filter(user => user.room === room)