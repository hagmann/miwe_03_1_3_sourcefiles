document.addEventListener("DOMContentLoaded", () => {
  const feedback = document.getElementById("feedback");
  const message = document.getElementById("message");
  const formChat = document.getElementById("chat");
  const notifications = document.getElementById("notifications");
  const messages = document.getElementById("messages");
  const selectedRoom = document.getElementById("currentRoom");
  const selectedUser = document.getElementById("currentUser");

  // Get username and room from URL
  const queryString = window.location.search; // Liefert beispielsweise '?username=john&room=chatroom'

  // Verwenden von URLSearchParams
  const params = new URLSearchParams(queryString);

  // Abrufen der Werte
  const name = params.get("username");
  const room = params.get("room");

  // Überprüfen, ob Benutzername und Raum vorhanden sind
  if (!name || !room) {
    // Benutzername oder Raum fehlen; Umleitung zur Login-Seite
    window.location.href = "/index.html";
    return;
  }

  const socket = io();

  // Join chatroom
  socket.emit("login", { name, room }, (response) => {
    selectedRoom.innerHTML = response.room;
    selectedUser.innerHTML = response.name;
  });


  // local event listeners

  formChat.addEventListener("submit", (e) => {
    e.preventDefault();
    socket.emit("sendMessage", e.target.message.value);
    e.target.message.value = "";
  });

  message.addEventListener("keypress", () => socket.emit("typing", name));

  // Socket.io listeners

  socket.on("notification", msg => notifications.innerHTML += `<div><strong>${msg.title}</strong>: <span>${msg.description}</span></div>`);

  socket.on("message", ({ user, text }) => {
    const messageHTML = `<li class="${user === name ? "own" : ""}">${user !== name ? `${user}: ` : ""} ${text}</li>`;
    messages.insertAdjacentHTML("beforeend", messageHTML);
    feedback.innerHTML = ""; // Leere das Feedback
  });

  socket.on("typing", function (user) {
    feedback.innerHTML = "<p><em>" + user + " is typing a message...</em></p>";
    setTimeout(() => {
      feedback.innerHTML = "";
    }, 3000);
  });
});
